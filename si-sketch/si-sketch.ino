#include <FAB_LED.h>

#include "GameState.h"

GameState gs;

void setup() {
	Serial.begin(57600);
}

void loop() {
	noInterrupts();
	gs.process();
	interrupts();
	delay(1);
}
