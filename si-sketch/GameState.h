#include "Buttons.h"
#include "Leds.h"

#define CALL_MEMBER_FN(pObject,ptrToMember)  ((pObject)->*(ptrToMember))

class GameState{
private:
	typedef void (GameState::*PROCESSFUNC)();
	typedef Buttons::BUTTON BUTTON;
	typedef Buttons::BUTTONS BUTTONS;
	
	typedef struct _STATEFUNC {
		PROCESSFUNC	f;
		float time;
		char name[20];
	} STATEFUNC;
	
	typedef struct _BUTTONLED {
		BUTTON button;
		Leds::UID led;
		Leds::COLOR color;
	} BUTTONLED;
	
	Buttons buttons;
	Leds leds;
	
	BUTTONS playersInGame;
	BUTTON currentPlayer;

	STATEFUNC stateFunc[8] = {
		{&GameState::processNone,		1/5000.0f,	"none"		},
		{&GameState::processLoading,	1/2000.0f,	"loading"	},
		{&GameState::processIdle,		0.0f,		"idle"		},
		{&GameState::processReading,	1/20000.0f,	"reading"	},
		{&GameState::processWaiting,	1/7000.0f,	"waiting"	},
		{&GameState::processWrong,		1/2000.0f,	"wrong"		},
		{&GameState::processRight,		1/2000.0f,	"right"		},
		{0}
	};
	STATEFUNC *currentState;
	STATEFUNC *oldState;
	unsigned long frameTime;
	unsigned long stateStarted;
	float progress;
	
	BUTTONLED buttonLeds[6] = {
		{BUTTON::YES,		0, {{0x00}, {0xFF}, {0x00}}},
		{BUTTON::NO,		1, {{0xFF}, {0x00}, {0x00}}},
		{BUTTON::PLAYER1,	2, {{0x00}, {0xFF}, {0xFF}}},
		{BUTTON::PLAYER2,	3, {{0xFF}, {0x00}, {0xFF}}},
		{BUTTON::PLAYER3,	4, {{0xFF}, {0xFF}, {0x00}}},
		{BUTTON::NONE, 0}
	};
	Leds::UID colorBallLed = 5;
	
public:
	GameState():
		currentState(stateFunc),
		oldState(stateFunc)
	{
		BUTTONLED *bl = buttonLeds;
		while(BUTTON::NONE != bl->button){
			leds.set(bl->led, bl->color);
			bl ++;
		}
	}
	
	void process() {
		buttons.process();
		frameTime = millis();
		if(oldState != currentState){
			stateStarted = frameTime;
			Serial.write(currentState->name);
			Serial.write("\r\n");
		}
		oldState = currentState;
		
		CALL_MEMBER_FN(this, currentState->f)();
		
		BUTTONLED *bl = buttonLeds;
		while(BUTTON::NONE != bl->button){
			leds.turn(bl->led, buttons.enabled(bl->button));
			bl ++;
		}

		Leds::COLOR ballColor = leds.black;
		if (currentState->f == &GameState::processLoading){
			ballColor = leds.blink(leds.white, time(), 300);
		} else if (currentState->f == &GameState::processReading){
			ballColor = leds.white;
		} else if (currentState->f == &GameState::processWaiting){
			BUTTONLED *bl = buttonLeds;
			while(BUTTON::NONE != bl->button){
				if(currentPlayer == bl->button) {
					ballColor = bl->color;
					break;
				}
				bl ++;
			}
		} else if (currentState->f == &GameState::processRight){
			ballColor = leds.blink({0x00, 0xFF, 0x00}, time(), 300);
		} else if (currentState->f == &GameState::processWrong){
			ballColor = leds.blink({0xFF, 0x00, 0x00}, time(), 300);
		}
		leds.set(colorBallLed, ballColor);
		
		leds.process();
	}
		
private:


	void processNone() {
		playersInGame = BUTTON::PLAYERS;
		changeState(&GameState::processLoading, BUTTON::NONE);
	}

	void processLoading() {
		if(nextState(&GameState::processIdle, BUTTON::YESNO)){
		}
	}
	
	void processIdle() {
		if(buttons.pressed(BUTTON::ANY)){
			changeState(&GameState::processReading, playersInGame);
		}
	}
	void processReading() {
		if(nextState(&GameState::processIdle, BUTTON::YESNO)){
			if(buttons.pressed(BUTTON::ANY)){
				if(buttons.pressed(BUTTON::PLAYER1)){
					currentPlayer = BUTTON::PLAYER1;
				} else if(buttons.pressed(BUTTON::PLAYER2)){
					currentPlayer = BUTTON::PLAYER2;
				} else if(buttons.pressed(BUTTON::PLAYER3)){
					currentPlayer = BUTTON::PLAYER3;
				}
				changeState(&GameState::processWaiting, BUTTON::YESNO|currentPlayer);
			}
		}
	}
	void processWaiting() {
		if(nextState(&GameState::processWrong, BUTTON::NONE)){
			if(buttons.pressed(BUTTON::YES)){
				changeState(&GameState::processRight, BUTTON::NONE);
			} else if(buttons.pressed(BUTTON::NO)){
				playersInGame &= ~currentPlayer;
				changeState(&GameState::processWrong, BUTTON::NONE);
			}
		}
	}
	void processWrong() {
		PROCESSFUNC next = &GameState::processReading;
		BUTTONS btns = playersInGame;
		if(0 == playersInGame ){
			next = &GameState::processIdle;
			playersInGame = Buttons::PLAYERS;
			btns = BUTTON::YESNO;
		}
		if(nextState(next, btns)){
		}
	}
	void processRight() {
		if(nextState(&GameState::processIdle, BUTTON::YESNO)){
			playersInGame = BUTTON::PLAYERS;
		}
	}

	void changeState(PROCESSFUNC s, BUTTONS enabledButtons) {
		currentState = stateFunc;
		buttons.resetState();
		buttons.set(enabledButtons);
		while(currentState->f) {
			if( s == currentState->f ) {
				break;
			}
			currentState ++;
		}
	}
	
	unsigned long time(){
		return frameTime - stateStarted;
	}
	
	float calcProgress(){
		float ret = currentState->time*GameState::time();
		if ( ret > 1.0f )
		{
			ret = -1.0f;
		}
		return ret;
	}
	
	bool nextState(PROCESSFUNC s, BUTTONS enabledButtons){
		progress = calcProgress();
		if(progress >= 0.0f){
			return true;
		}else{
			changeState(s, enabledButtons);
			return false;
		}				
	}
};
