class Buttons {
public:
	typedef unsigned long BUTTONS;
	typedef enum _BUTTON {
		NONE = 0x00,
		YES = 0x01,
		NO = 0x02,
		PLAYER1 = 0x04,
		PLAYER2 = 0x08,
		PLAYER3 = 0x10,
		YESNO = YES|NO,
		PLAYERS = PLAYER1|PLAYER2|PLAYER3,
		ALL = 0xFF,
		ANY = 0xFF
	} BUTTON;
	
	Buttons():
		state(NONE),
		isEnabled(NONE)
	{
		BUTTONINFO *bi = buttons;
		while(bi->b) {
			pinMode(bi->in, INPUT_PULLUP);
			bi++;
		}
	}

	bool enabled(BUTTONS b) {
		return (isEnabled & b) != 0;
	}
	
	bool pressed(BUTTONS b) {
		return (isEnabled & state & b) != 0;
	}
	
	BUTTONS pressed() {
		return isEnabled & state;
	}
	void resetState() {
		state = BUTTON::NONE;
	}
	
	void process() {
		BUTTONINFO *bi = buttons;
		while(bi->b) {
			if(isEnabled&bi->b && LOW==digitalRead(bi->in)) {
				state |= bi->b;
			}
			bi++;
		}
	}
	
	void set(BUTTONS b){
		isEnabled = b;
	}
	
	void disable(BUTTONS b){
		isEnabled &= ~b;
	}
	
	void enable(BUTTONS b){
		isEnabled |= b;
	}
	
private:
	unsigned long isEnabled;
	typedef unsigned char PIN;
	typedef struct {
		BUTTON b;
		PIN in;
	} BUTTONINFO;
	BUTTONINFO buttons[6] = {
		{YES,		5},
		{NO,		7},
		{PLAYER1,	2},
		{PLAYER2,	3},
		{PLAYER3,	4},
		{NONE, 		0}
	};
	BUTTONS state;
};