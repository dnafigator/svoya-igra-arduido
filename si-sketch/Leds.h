#include <FAB_LED.h>
#define PIXEL_COUNT 6

class Leds{
public:
	typedef struct _COLOR{
		uint8_t r;
		uint8_t g;
		uint8_t b;
	} COLOR;
	COLOR black = {0x00, 0x00, 0x00};
	COLOR white = {0xFF, 0xFF, 0xFF};

	Leds(){
	}
	
	typedef unsigned char UID;

	void set(UID index, COLOR color) {
		pixels[index].r = color.r;
		pixels[index].g = color.g;
		pixels[index].b = color.b;
		colors[index] = pixels[index];
	}
	
	void turn(UID index, bool turn) {
		if(turn){
			pixels[index] = colors[index];
		} else {
			pixels[index] = {0, 0, 0};
		}
	}
	
	void process(){
		leds.sendPixels(PIXEL_COUNT, pixels);
		log();
	}
	
	void log() {
		static unsigned long cur = 0;
		if ( millis() - cur < 1000) {
			return;
		}
		cur = millis();
		char out[0x100];
		char out2[0x10];
		*out = 0;
		for(int i = 0; i < PIXEL_COUNT; i ++ ) {
			sprintf(out2, "0x%02X%02X%02X ", pixels[i].r, pixels[i].g, pixels[i].b);
			strcat(out, out2);
		}
		strcat(out, "\r\n");
		Serial.write(out);
	}

	COLOR blink(COLOR color, unsigned long period, unsigned long time){
		float mul = (1 - cosf( (float)time / (float)period * PI * 2 )) / 2;
		color.r *= mul;
		color.g *= mul;
		color.b *= mul;
		return color;
	};

private:
	apa106<D,6> leds;
	rgb pixels[PIXEL_COUNT];
	rgb colors[PIXEL_COUNT];
};